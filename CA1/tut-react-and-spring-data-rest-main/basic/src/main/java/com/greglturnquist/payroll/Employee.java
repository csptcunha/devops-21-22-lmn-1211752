/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.greglturnquist.payroll;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * @author Greg Turnquist
 */
// tag::code[]
@Entity // <1>
public class Employee {

    /** Attributes **/
    private @Id
    @GeneratedValue
    Long id; // <2>
    private String firstName;
    private String lastName;
    private String description;
    private String jobTitle;
    private int jobYears;
    private String email;

    /** Constructor **/
    public Employee(String firstName, String lastName, String description, String jobTitle, int jobYears, String email) {
        if (!this.validateFirstName(firstName)){
            throw new IllegalArgumentException("First name must have more than 2 characters");
        }

        if (!this.validateLastName(lastName)){
            throw new IllegalArgumentException("Last name must have more than 2 characters");
        }

        if (!this.validateDescription(description)){
            throw new IllegalArgumentException("Description must have more than 3 characters");
        }

        if (!this.validateJobTitle(jobTitle)){
            throw new IllegalArgumentException("Job Title must have more than 3 characters");
        }

        if (!this.validateJobYears(jobYears)){
            throw new IllegalArgumentException("Job Years must be bigger than 0");
        }

        if (!this.validateEmail(email)){
            throw new IllegalArgumentException("Email must have a @ sign");
        }

        this.firstName = firstName;
        this.lastName = lastName;
        this.description = description;
        this.jobTitle = jobTitle;
        this.jobYears = jobYears;
        this.email = email;
    }

    /** Getters and Setters **/
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public int getJobYears() {
        return jobYears;
    }

    public void setJobYears(int jobYears) {
        this.jobYears = jobYears;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /** Operators **/
    private boolean validateFirstName (String firstName){
        return firstName != null && firstName.length() > 2;
    }

    private boolean validateLastName (String lastName){
        return lastName != null && lastName.length() > 2;
    }

    private boolean validateDescription (String description){
        return description != null && description.length() > 3;
    }

    private boolean validateJobTitle (String title){
        return title != null && title.length() > 3;
    }

    private boolean validateJobYears (int years){
        return years >= 0;
    }

    private boolean validateEmail(String email) {
        return email.contains("@") && email.contains(".");
    }


    /** Overrides **/
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof Employee)) return false;
        if (!super.equals(object)) return false;
        Employee employee = (Employee) object;
        return getJobYears() == employee.getJobYears() &&
                java.util.Objects.equals(getId(), employee.getId()) &&
                java.util.Objects.equals(getFirstName(), employee.getFirstName()) &&
                java.util.Objects.equals(getLastName(), employee.getLastName()) &&
                java.util.Objects.equals(getDescription(), employee.getDescription()) &&
                java.util.Objects.equals(getJobTitle(), employee.getJobTitle());
    }

    public int hashCode() {
        return Objects.hash(super.hashCode(), getId(), getFirstName(), getLastName(), getDescription(), getJobTitle(), getJobYears());
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", description='" + description + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", jobYears='" + jobYears + '\'' +
                '}';
    }
}
// end::code[]
