import com.greglturnquist.payroll.Employee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class EmployeeTest {

    @Test
    void getFirstNameSuccess() {
        String firstName = "Carlos";
        String lastName = "Cunha";
        String description = "Developer";
        String jobTitle = "Switch Student";
        int jobYears = 10;
        String email = "carlos@gmail.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);

        String expected = "Carlos";
        String result = employee.getFirstName();

        assertEquals(expected,result);
    }

    @Test
    void getLastNameSuccess() {
        String firstName = "Carlos";
        String lastName = "Cunha";
        String description = "Developer";
        String jobTitle = "Switch Student";
        int jobYears = 10;
        String email = "carlos@gmail.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);

        String expected = "Cunha";
        String result = employee.getLastName();

        assertEquals(expected,result);
    }

    @Test
    void getDescriptionSuccess() {
        String firstName = "Carlos";
        String lastName = "Cunha";
        String description = "Developer";
        String jobTitle = "Switch Student";
        int jobYears = 10;
        String email = "carlos@gmail.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);

        String expected = "Developer";
        String result = employee.getDescription();

        assertEquals(expected,result);
    }

    @Test
    void getJobTitleSuccess() {
        String firstName = "Carlos";
        String lastName = "Cunha";
        String description = "Developer";
        String jobTitle = "Switch Student";
        int jobYears = 10;
        String email = "carlos@gmail.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);

        String expected = "Switch Student";
        String result = employee.getJobTitle();

        assertEquals(expected,result);
    }

    @Test
    void getJobYearsSuccess() {
        String firstName = "Carlos";
        String lastName = "Cunha";
        String description = "Developer";
        String jobTitle = "Switch Student";
        int jobYears = 10;
        String email = "carlos@gmail.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);

        int expected = 10;
        int result = employee.getJobYears();

        assertEquals(expected,result);
    }

    @Test
    void getEmailSuccess() {
        String firstName = "Carlos";
        String lastName = "Cunha";
        String description = "Developer";
        String jobTitle = "Switch Student";
        int jobYears = 10;
        String email = "carlos@gmail.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);

        String expected = "carlos@gmail.com";
        String result = employee.getEmail();

        assertEquals(expected,result);
    }

    @Test
    void newEmployeeFailFirstNameInvalid() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Ca";
            String lastName = "Cunha";
            String description = "Developer";
            String jobTitle = "Switch Student";
            int jobYears = 10;
            String email = "carlos@gmail.com";
            Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);
        });
        assertEquals("First name must have more than 2 characters", thrown.getMessage());
    }

    @Test
    void newEmployeeFailLastNameInvalid() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Carlos";
            String lastName = "Cu";
            String description = "Developer";
            String jobTitle = "Switch Student";
            int jobYears = 10;
            String email = "carlos@gmail.com";
            Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);
        });
        assertEquals("Last name must have more than 2 characters", thrown.getMessage());
    }

    @Test
    void newEmployeeFailDescriptionInvalid() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Carlos";
            String lastName = "Cunha";
            String description = "Dev";
            String jobTitle = "Switch Student";
            int jobYears = 10;
            String email = "carlos@gmail.com";
            Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);
        });
        assertEquals("Description must have more than 3 characters", thrown.getMessage());
    }

    @Test
    void newEmployeeFailJobTitleInvalid() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Carlos";
            String lastName = "Cunha";
            String description = "Developer";
            String jobTitle = "Swi";
            int jobYears = 10;
            String email = "carlos@gmail.com";
            Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);
        });
        assertEquals("Job Title must have more than 3 characters", thrown.getMessage());
    }

    @Test
    void newEmployeeFailJobYearsZero() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Carlos";
            String lastName = "Cunha";
            String description = "Developer";
            String jobTitle = "Switch Student";
            int jobYears = 0;
            String email = "carlos@gmail.com";
            Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);
        });
        assertEquals("Job Years must be bigger than 0", thrown.getMessage());
    }

    @Test
    void newEmployeeFailJobYearNegative() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Carlos";
            String lastName = "Cunha";
            String description = "Developer";
            String jobTitle = "Switch Student";
            int jobYears = -1;
            String email = "carlos@gmail.com";
            Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);
        });
        assertEquals("Job Years must be bigger than 0", thrown.getMessage());
    }

    @Test
    void newEmployeeFailEmailInvalid() {
        Exception thrown = Assertions.assertThrows(Exception.class, () -> {
            String firstName = "Carlos";
            String lastName = "Cunha";
            String description = "Developer";
            String jobTitle = "Switch Student";
            int jobYears = 10;
            String email = "carlosgmail.com";
            Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);
        });
        assertEquals("Email must have a @ sign", thrown.getMessage());
    }

    @Test
    void testHashCode() {
        String firstName1 = "Carlos";
        String lastName1 = "Cunha";
        String description1 = "Developer";
        String jobTitle1 = "Switch Student";
        int jobYears1 = 10;
        String email1 = "carlos@gmail.com";
        String firstName2 = "Hugo";
        String lastName2 = "Martins";
        String description2 = "QA";
        String jobTitle2 = "IT Developer";
        int jobYears2 = 20;
        String email2 = "hugo@gmail.com";
        Employee employee1 = new Employee(firstName1,lastName1,description1,jobTitle1,jobYears1, email1);
        Employee employee2 = new Employee(firstName2,lastName2,description2,jobTitle2,jobYears2, email2);
        Employee x = employee1;
        assertNotEquals(employee1,employee2);
        assertEquals(employee1.hashCode(),x.hashCode());
    }

    @Test
    void testToString() {
        String firstName = "Carlos";
        String lastName = "Cunha";
        String description = "Developer";
        String jobTitle = "Switch Student";
        int jobYears = 10;
        String email = "carlos@gmail.com";
        Employee employee = new Employee(firstName,lastName,description,jobTitle,jobYears, email);

        String expected = "Employee{id=null, firstName='Carlos', lastName='Cunha', description='Developer', jobTitle='Switch Student', jobYears='10', email='carlos@gmail.com'}";
        String result = employee.toString();

        assertEquals(expected,result);
    }

}
