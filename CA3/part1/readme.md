>> # DEVOPS

> ## CA3 (class assignment 3)


>### Parte 1

Virtualização - Criar uma VM e configurá-la de raiz.


> >**Passo 1.1** - Criar a Máquina Virtual

Recorrer ao link: 

https://moodle.isep.ipp.pt/pluginfile.php/211676/mod_resource/content/1/devops05.pdf

e seguir os passos para a instalação de uma VM através do VirtualBox com as seguintes características:

- 2 GB de RAM;
- 10 GB de armazenamento;
- adaptador 1 como NAT;
- adaptador 2 como host-only com o IP 192.168.56.1/24;

Depois de instalar todas as aplicações (net-tools ssh, git, java*), ligar a VM criada.

*(substituir a instalação do jdk-8 pela versão 11).


> **Passo 1.2** - Criação do diretório CA3 dentro do diretório /devops-21-22-lmn-1211769

Abrir o terminal/linha de comandos da máquina local e mudar para o diretório pretendido:

`§ cd devops-21-22-lmn-1211769/` - Este comando dá acesso ao diretório pretendido.

`§ mkdir CA3` - Criação de novo diretório CA3 com finalidade de guardar todos os ficheiros necessários para
este exercício.

`§ cd CA3` - Para entrar na pasta acabada de criar.

`§ mkdir part1` - Criação de novo diretório part1 com a mesma finalidade já referida anteriormente.

> **Passo 1.3** - Clonar o repositório utilizado no CA1 para a VM

`§ git clone https://github.com/spring-guides/tut-react-and-spring-data-rest`

Nota: Uma vez que estava a ter problemas com o meu repositório decidi, para efeitos deste exercício, fazer clone
novamente do repositório fornecido para o CA1 para poder concluir o exercício 3 parte 1.

> **Passo 1.4** - Correr o spring boot

Antes de correr a aplicação, garantir que as permissões de execução do Maven Wrapper estão ok

`$ chmod u+x mvnw`

Dentro da VM, ir para o diretório tut-react-and-spring-data-rest/basic/.

`$ ./mvnw spring-boot:run`

Acedendo ao endereço
http://192.168.56.5:8080/ como seria expectável, é visível a informação pretendida.

![](output.png)

> **Passo 1.5** - Clonar o repositório utilizado no CA2 para a VM (e também para a máquina local)

Nota: Uma vez que estava a ter problemas com o meu repositório decidi, para efeitos deste exercício, fazer clone
novamente do repositório fornecido para o CA2 para poder concluir o exercício 3 parte 1.

`§ git clone https://jborgespinheiro@bitbucket.org/luisnogueira/gradle_basic_demo.git`


> **Passo 1.6** - Correr a ChatApp (CA2-part1)

Será executado o servidor na VM e parte do cliente na máquina local, isto porque desta forma conseguir-se-á executar a parte que 
necessita de GUI na máquina local, evitando assim problemas com a VM que não possui essa capacidade.


Para que seja possível as duas máquinas comunicarem, é necessário fazer alterações:

Na task runClient, onde se encontra escrito 

*args 'localhost', '59001'*

alterar para:

**args '192.168.56.5', '59001'** - é necessária esta alteração para permitir à máquina local conhecer o IP da VM.


Na linha de comandos da VM, ir para o diretório /gradle_basic_demo e executar os comandos:

`$ ./gradlew build` 

`$ ./gradlew runServer` 

Na linha de comandos da máquina local, ir para o diretório /gradle_basic_demo e executar os comandos:

`$ gradle build`

`$ gradle runClient` 

Nota: Uma vez que na máquina local, o gradle se encontra instalado, não necessitamos de recorrer ao wrapper.

> **Passo 1.7** - Correr CA2-part2

Nota: Copiei o repositório abaixo indicado para a VM:

`§ git clone https://jborgespinheiro@bitbucket.org/jborgespinheiro/devops-21-22-lmn-1211769.git`

- chmod u+x gradlew (first I need to grant permission)

No terminal, aceder à VM e ir para o diretório CA2/part2/react-and-spring-data-rest-basic

`$ ./gradlew build`

`$ ./gradlew bootRun`

Acedendo ao endereço
http://192.168.56.5:8080/ como seria expectável, é visível a informação pretendida.

![](output.png)

> **Passo 1.8** - Enviar as alterações efectuadas

`§ git add .`

`§ git status`

`§ git tag ca3-part1`

`§ git commit -m "updated readme.md resolves #18 #19" `

`§ git push origin ca3-part1`

