
>> # DEVOPS

> ## CA3 (class assignment 3)


>### Parte 2

Virtualização - Criar dois VM's recorrendo ao software Vagrant.

>**Passo 1.1** - Criar pasta CA3/part2 e readme.md

Abrir o terminal/linha de comandos da máquina local e mudar para o diretório pretendido:

`§ cd devops-21-22-lmn-1211769/CA3` - Este comando dá acesso ao diretório pretendido.

`§ mkdir part2`- Para criar a pasta com o propósito de guardar os documentos referentes 
à resolução deste exercício

`§ nano readme.md` - para criar o ficheiro readme

>**Passo 1.2** - Instalar o software Vagrant

No terminal de comandos, digitar o seguinte comando:

`§ brew install vagrant`

Para confirmar que a instalação foi bem sucedida e que versão foi instalada, executar:

`§ vagrant -v` 


>**Passo 1.3** - Clonar o repositório disponibilizado para o diretório CA3/part2

Aceder ao diretório /CA3/part2 e executar o seguinte comando

`§ git clone https://jborgespinheiro@bitbucket.org/atb/vagrant-multi-spring-tut-demo.git`



>**Passo 1.4** - Copiar a pasta CA2/part2/react-and-spring-data-rest-basic para CA3/part2

Na linha de comandos, executar copiar os ficheiros através do comando `§ cp`



>**Passo 1.5** - Abrir o ficheiro Vagrantfile no IntelliJ

Alterar o ficheiro para que o mesmo, quando executado, corra a minha versão gradle já presente no repositório.

    Change the following command to clone your own repository!

      git clone https://jborgespinheiro@bitbucket.org/jborgespinheiro/devops-21-22-lmn-1211769.git
      cd devops-21-22-lmn-1211752/CA3/part2/react-and-spring-data-rest-basic

Alterar a versão do java para versão 11, uma vez que  nossa aplicação está configurada nessa versão.

    sudo apt-get install openjdk-11-jdk-headless -y

Uma vez que foi necessária fazer esta alteração, a versão ubuntu necessita de ser alterada porque a **ubuntu-xenial** 
não é compatível com java 11.

Desta forma, devem ser alteradas todas as configurações das boxes para 

**ubuntu/bionic64**

É necessário também alterar o caminho do war file:


    sudo cp ./build/libs/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war /var/lib/tomcat8/webapps

>**Passo 1.6** - Correr o ficheiro vagrant para criar as duas VMs

Antes de iniciar as 2 VMs, adiciona-se as alterações feitas no repositório:

`§ git add .`

`§ git commit -m "vagrantfile updated resolves #21"`

`§ git push`

`§ vagrant up`


Nota: Foi necessário remover as VM criadas uma vez que houve um erro ao clonar o repositório pois o
ficheiro .jar estava a ser ignorado pelo ficheiro .gitignore

Assim, primeiro destruiu-se as VM:

`§ vagrant destroy db`

`§ vagrant destroy web`

E posteriormente resolver a questão do gradle wrapper.

`§ git add -f gradle/wrapper/gradle-wrapper.jar`

`§ git commit -m "solving some issues"`

`§ git push`


Executar novamente o comando para inicializar as 2 VMs:

`§ vagrant up` 

Para confirmar que as VM estão a correr:

`§ vagrant global-status`

>**Passo 1.7** - Alterações à aplicação react-and-sprint-data-rest-basic tendo em consideração 
> os commits feitos no repositório do Professor ATB

Aceder através do link:

https://jborgespinheiro@bitbucket.org/atb/tut-basic-gradle.git

### Alterações a fazer:

- No ficheiro build.gradle:

No frontend ---> assembleScript = 'run webpack'

Adicionar plugin ---> id 'war'

Adicionar dependência ---> providedRuntime 'org.springframework.boot:spring-boot-starter-tomcat'

- No ficheiro package.json:

Adicionar script ---> "watch": "webpack --watch -d"

- Adicionar class ServletInitializer e fazer as alterações necessárias na class DatabaseLoader
e Employee


- No ficheiro application.properties adicionar :

server.servlet.context-path=/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT
spring.data.rest.base-path=/api
spring.datasource.url=jdbc:h2:tcp://192.168.56.11:9092/./jpadb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE (changed IP to "192.168.56.11" since it is the one defined in the Vagrant file)
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
spring.jpa.hibernate.ddl-auto=update
spring.h2.console.enabled=true
spring.h2.console.path=/h2-console
spring.h2.console.settings.web-allow-others=true


- No ficheiro app.js alterar:


    componentDidMount() { 
        client({method: 'GET', path: '/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/api/employees'}).done(response => {
            this.setState({employees: response.entity._embedded.employees});
        });
     }

- No ficheiro index.html alterar 

(retirar a barra inclinada antes do main.css)

 <link rel="stylesheet" href="main.css" />


>**Passo 1.8** - Testar a aplicação na web

http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/ 

http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console 


Inserir no campo JBDC:

jdbc:h2:tcp://192.168.56.11:9092/./jpadb

Posteriormente inserir uma nova linha na tabela:

INSERT INTO EMPLOYEE (ID, DESCRIPTION, EMAIL, FIRST_NAME, JOB_TITLE, JOB_YEARS,
LAST_NAME) VALUES (2, 'hobbit', 'hobbit@shire.com', 'sam', 'helper', 50, 
'BagginsToBe')


Voltar a carregar o URL:

http://192.168.56.10:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/

e perceber que as alterações feitas ficaram gravadas na tabela.

![](tabela.png)

>**Passo 1.9** - Commit das alterações

`§ git add .`

`§ git commit -m "updating readme file"`

`§ git push`

`§ git tag ca3-part2`

`§ git push origin ca3-part2`
