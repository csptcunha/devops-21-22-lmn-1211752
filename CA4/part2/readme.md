#CA4, Part 2 - Containers with Docker

## Part II
#### Step 1 - Create directory (part2; part2/web; part2/db and part2/data) and readme file inside CA4
* `§ mkdir part2`
* `§ cd part2`
* `§ touch readme.md`
* `§ mkdir db`
* `§ mkdir web`
* `§ mkdir data`

#### Step 2 - Create docker-compose.yml file
* Created a new file via intellij with .yml 

#### Step 3 - Created Docker files for both web and db directories
* `§ cd CA4/part2/web`
* `§ touch Dockerfile`
* `§ cd CA4/part2/db`
* `§ touch Dockerfile`

#### Step 4 - Make necessary changes on Dockerfiles and docker-compose.yml files
* see corresponding files for the modifications made

#### Step 5 - Force to build gradle on react-and-spring-data-rest-basic
* `§ ./gradlew build` -> this forces gradle to build
* Commited the new files

#### Step 6 - Force to insert .war file 

* On react-and-spring-data-rest-basic/build/libs and run

* `§ git add -f react-and-spring-data-rest-basic-0.0.1-SNAPSHOT.war`

#### Step 7 - Create docker container
* `§ docker-compose build --no-cache` -> build the container from scratch
* `§ docker-compose up` -> create image

## Check functioning Db and Web 
#### Step 8 - Web check
* run http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/ on Google Chrome
* Check if working as expected
![](web_screenshot.png)

#### Step 9 - Db check
* run http://localhost:8080/react-and-spring-data-rest-basic-0.0.1-SNAPSHOT/h2-console on Google Chrome
* Insert on URL: jdbc:h2:tcp://192.168.56.11:9092/./jpadb
* Test Connection and then Connect
* Click on the table employee
* Ctrl + Enter
* Check if working as expected
![](db_screeenshot.png)

#### Step 10 - Tag the db and web images to DockerHub
**Tag Web**
* `§ docker login`
* `§ docker tag part2_web 1211752/part2_web:1.0`
* `§ docker push 1211752/part2_web:1.0`

**Tag DB**
* `§ docker login`
* `§ docker tag part2_db 1211752/part2_db:1.0`
* `§ docker push 1211752/part2_db:1.0`

#### Step 11 - Copy the database file from the container to the shared folder in the host machine
* `§docker-compose up`
* `§ docker-compose exec db bash`
* `§ pwd`
* `§ ls -la`
* `§ cp jpadb.mv /usr/src/data-backup`

#### Step 12 - Last commit of the project
* `§ git add .`
* `§ git commit -m "commit ultimate changes of tag and copy the file from the db container to host machine shared folder"`
* `§ git push`
* `§ git tag ca4-part2`
* `§ git push origin ca4-part2`

# Kubernetes Alternative

## Docker vs Kubernetes

If cloud-native technologies and containers are on your radar, you’ve likely encountered Docker and Kubernetes and might
be wondering how they relate to each other. Is it Kubernetes vs Docker or Kubernetes and Docker—or both?

In a nutshell, Docker is a suite of software development tools for creating, sharing and running individual containers; 
Kubernetes is a system for operating containerized applications at scale.

Think of containers as standardized packaging for microservices with all the needed application code and dependencies 
inside. Creating these containers is the domain of Docker. A container can run anywhere, on a laptop, in the cloud, on 
local servers, and even on edge devices.

A modern application consists of many containers. Operating them in production is the job of Kubernetes. Since containers
are easy to replicate, applications can auto-scale: expand or contract processing capacities to match user demands.

Docker and Kubernetes are mostly complementary technologies—Kubernetes and Docker. However, Docker also provides a system
for operating containerized applications at scale, called Docker Swarm—Kubernetes vs Docker Swarm. Let’s unpack the ways 
Kubernetes and Docker complement each other and how they compete.

### **What is Docker?**

It is a suite of tools for developers to build, share, run and orchestrate containerized apps.

**Developer tools for building container images:** Docker Build creates a container image, the blueprint for a container, 
including everything needed to run an application – the application code, binaries, scripts, dependencies, configuration,
environment variables, and so on. Docker Compose is a tool for defining and running multi-container applications. These 
tools integrate tightly with code repositories (such as GitHub) and continuous integration and continuous delivery (CI/CD)
pipeline tools (such as Jenkins).

**Sharing images:** Docker Hub is a registry service provided by Docker for finding and sharing container images with your 
team or the public. Docker Hub is similar in functionality to GitHub.

**Running containers:** Docker Engine is a container runtime that runs in almost any environment: Mac and Windows PCs, Linux
and Windows servers, the cloud, and on edge devices. Docker Engine is built on top containerd, the leading open-source 
container runtime, a project of the Cloud Native Computing Foundation (DNCF).

**Built-in container orchestration:** Docker Swarm manages a cluster of Docker Engines (typically on different nodes) called
a swarm. Here the overlap with Kubernetes begins.

### **What is Kubernetes?**

Kubernetes is an open-source container orchestration platform for managing, automating, and scaling containerized 
applications. Although Docker Swarm is also an orchestration tool, Kubernetes is the de facto standard for container 
orchestration because of its greater flexibility and capacity to scale.

Organizations use Kubernetes to automate the deployment and management of containerized applications. Rather than 
individually managing each container in a cluster, a DevOps team can instead tell Kubernetes how to allocate the 
necessary resources in advance.

Where Kubernetes and the Docker suite intersect is at container orchestration. So when people talk about Kubernetes vs. 
Docker, what they really mean is Kubernetes vs. Docker Swarm.

### **What are the challenges of container orchestration?**

Although Docker Swarm and Kubernetes both approach container orchestration a little differently, they face the same 
challenges. A modern application can consist of dozens to hundreds of containerized microservices that need to work 
together smoothly. They run on multiple host machines, called nodes. Connected nodes are known as a cluster.

**Container deployment** In the simplest terms, this means to retrieve a container image from the repository and deploy 
it on a node. However, an orchestration platform does much more than this: it enables automatic re-creation of failed 
containers, rolling deployments to avoid downtime for the end-users, as well as managing the entire container lifecycle.
Scaling. This is one of the most important tasks an orchestration platform performs. The “scheduler” determines the 
placement of new containers so compute resources are used most efficiently. Containers can be replicated or deleted on 
the fly to meet varying end-user traffic.

**Networking** The containerized services need to find and talk to each other in a secure manner, which isn’t a trivial 
task given the dynamic nature of containers. In addition, some services, like the front-end, need to be exposed to 
end-users, and a load balancer is required to distribute traffic across multiple nodes.

**Observability** An orchestration platform needs to expose data about its internal states and activities in the form of 
logs,events, metrics, or transaction traces. This is essential for operators to understand the health and behavior of the 
container infrastructure as well as the applications running in it.

**Security** Security is a growing area of concern for managing containers. An orchestration platform has various 
mechanisms built in to prevent vulnerabilities such as secure container deployment pipelines, encrypted network traffic, 
secret stores and more. However, these mechanisms alone are not sufficient, but require a comprehensive DevSecOps approach.
With these challenges in mind, let’s take a closer look at the differences between Kubernetes and Docker Swarm.

### **Docker and Kubernetes as complementary technologies**
Simply put, the Docker suite and Kubernetes are technologies with different scopes. You can use Docker without Kubernetes
and vice versa, however they work well together.

From the perspective of a software development cycle, Docker’s home turf is development. This includes configuring, 
building, and distributing containers using CI/CD pipelines and DockerHub as an image registry. On the other hand, 
Kubernetes shines in operations, allowing you to use your existing Docker containers while tackling the complexities of 
deployment, networking, scaling, and monitoring.

Although Docker Swarm is an alternative in this domain, Kubernetes is the best choice when it comes to orchestrating 
large distributed applications with hundreds of connected microservices including databases, secrets and external 
dependencies.

