# Docker Tutorial

## Part I

### PartA
#### Step 1 - Create new directory (CA4-part1):
* `§ mkdir CA4`
* `§ mkdir part1`
* `§ cd part1`

#### Step 2 - Create a new readme.md file and add to repository:
* `§ touch readme.md`
* `§ git add readme.md`
* `§ git commit -m ‘Implementation: create new directory CA2 and readme.md file [resolves #21]’`
* `§ git push`

#### Step 3 - Clone into CA4/part1 the repository that has the chat server (gradle basic demo):
* `§ git clone https://bitbucket.org/luisnogueira/gradle_basic_demo.git`

#### Step 4 - Delete .git directory and .gitignore file
* `§ cd gradle_basic_demo`
* `§ rm -rf .git` -> delete .git directory
* `§ rm .gitignore` -> delete .gitignore
* `§ ls -la` -> list all file to confirm I've successfully deleted

#### Step 5 - Create a dockerfile and add to directory
* Created a file called Dockerfile_versionA into my CA4/part1 in intellij
* See Dockerfile_versionA

#### Step 6 - Open a terminal at CA4/part1 and run this command to buil an image to container
* `§ docker build -t ca4part1 .` -> created an image called ca4part1

#### Step 7 - Create Docker account
* Created an account and made login

#### Step 8 - Container generation
* `§ docker run --name cont_part1 -p 59001:59001 -d ca4part1`

#### Step 9 - Add the new container to Docker
* `§ docker tag ca4part1 1211752/ca4part1`
* `§ docker push 1211752/ca4part1`

#### Step 10 - Commit Changes and tag issue
* `§ git add .`
* `§ git commit -m ‘Implementation: create container and pushed to Docker (resolves #23)’`
* `§ git push`

#### Step 11 - Run Chat Server
* `§ cd gradle_basic_demo`
* `§ ./gradlew runClient` -> confirm that chat server appears and runs without problems 

#### Step 12 - Make final commit with final tag
* `§ git add .`
* `§ git commit -m "Implementation: final commit and readme (resolves #24)"`
* `§ git push`
* `§ git tag ca4-part1_a`
* `§ git push origin ca4-part1_a`

### PartB
#### Step 1 - Create a dockerfile and add to directory
* Created a file called Dockerfile_versionB into my CA4/part1/partB in intellij
* See Dockerfile_versionB

#### Step 2 - Run the gradle_basic_demo
* `§ cd gradle_basic_demo`
* `§ ./gradlew clean build`

#### Step 3 - Open a terminal at CA4/part1/partB and run this command to buil an image to container
* `§ docker build -t ca4partb .` -> created an image called ca4partb

#### Step 4 - Container generation
* `§ docker run --name cont_partb -p 59001:59001 -d ca4partb`

#### Step 5 - Add the new container to Docker
* `§ docker tag ca4partb 1211752/ca4partb`
* `§ docker push 1211752/ca4partb`

-> After running confirmed in Docker if the image and container was successfully created and running

#### Step 6 - Run Chat Server
* `§ cd gradle_basic_demo`
* `§ ./gradlew runClient` -> confirm that chat server appears and runs without problems

#### Step 7 - Make final commit with final tag
* `§ cd ..`
* `§ git add .`
* `§ git commit -m "Implementation: final commit and readme of part B (resolves #25)"`
* `§ git push`
* `§ git tag ca4-part1_b`
* `§ git push origin ca4-part1_b`

##### General Search Commands :
* `§ docker images` -> listar as imagens criadas
* `§ docker ps` -> listar os containers criados

<br>

## Part II
#### Step 1 -

#### Step 2 -

#### Step 3 -

#### Step 4 -

#### Step 5 -

#### Step 6 -

#### Step 7 -

#### Step 8 -

#### Step 9 -

#### Step 10 -

#### Step 11 -

#### Step 12 -

#### Step 13 -

#### Step 14 -

#### Step 15 -

#### Step 16 -

#### Step 17 -

#### Step 18 -

#### Step 19 -

#### Step 20 -

#### Step 21 -