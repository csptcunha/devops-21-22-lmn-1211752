# Jenkins Tutorial

## Part I
#### Step 1 - Create new directory (CA5):
* `§ mkdir CA5/`

#### Step 2 - Change to the new directory:
* `§ cd CA5`

#### Step 3 - Create a new readme.md file and add to repository:
* `§ mkdir part1`
* `§ cd part1`
* `§ touch readme.md`
* `§ git add .`
* `§ git commit -m ‘Implementation: create new directory part1, part2 and readme.md file’`
* `§ git push`

#### Step 4 - Install jenkins
* I went to https://www.jenkins.io/download/ and clicked on "Generic Java Package (.war) "
  and saved the file inside CA5/jenkins, and then I run the command on the terminal:
* `§ java -jar jenkins.war --httpPort=9091` 

* Runned on port 9090 since the default was already in use.

#### Step 5 - Open localhost on the browser
* After the jenkins is up and runnning I went to localhost on my browser and logged in jenkins and chose the default
  pacakage.

#### Step 6 - Created a Jenkinsfile
* On CA2/CA2-partI/gradle_basic_demo I've added a Jenkinsfile with the necessary script to:
  * Checkout;
  * Assemble;
  * Test;
  * Archiving

(check Jenkinsfile's script)

#### Step 7 - Changes to .gitignore
* I needed to make changes to .gitignore file to don't ignore gradle folder which contains the gradle-wrapper.jar file. To
  do this I had to write "!gradle" and:
* `§ git add -f gradle-wrapper.jar`
* `§ git commit -m "commiting build folder"`
* `§ git push`

* I had to commit the build folder as well. For this I changed the directory to gradle_basic_demo and:
* `§ git add build`
* `§ git commit -m "commiting gradle-wrapper.jar"`
* `§ git push`

#### Step 8 - Created a new pipeline
* Created a new pipeline called "CA5-part1".
* Defined the "Pipeline Script from SCM" and Git was chosen as the SCM.
* Indicated my repository URL, and additionally the path to the Jenkinsfile located at CA2 - part1 folder
* After this, I pressed "Build Now" to build the project/job and checked the time and success of building it.
* Everytinhg worked as intended.

#### Step 9 - Commit final changes
* `§ git commit -a -m 'final commit'`
* `§ git push`

#### Step 10 - Tag the last commit to seal the project
* `§ git tag -a ca5-part1-final-tag -m 'final tag part1'`
* `§ git push origin ca5-part1-final-tag`
