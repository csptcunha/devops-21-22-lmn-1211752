
# CI/CD Pipelines with Jenkins

## Part II
#### Step 1 - Copy gradle_basic_demo into this directory
* Copied the folder from CA2/CA2-partI into my folder CA5/part2 to simplify the changes to be made.
* obs.: this folder has the changes made on CA5 part1. 

#### Step 2 - Add to Jenkinsfile -> Javadoc

stage('Javadoc') { 
    steps {
        echo 'Javadoc...'
            dir('CA5/part2/gradle_basic_demo') {
            sh './gradlew javadoc'
        }
        publishHTML([allowMissing: false,
            alwaysLinkToLastBuild: false,
            keepAll: false, reportDir: 'CA5/part2/gradle_basic_demo/build/docs/javadoc',
            reportFiles: 'index.html',
            reportName: 'HTML Report',
            reportTitles: 'The Report'])
        }
    }
}`

#### Step 3 - Add to Jenkinsfile -> Docker Image
`
stage('Docker Image'){
    steps {
        script {
            def customImage = docker.build("my-image:${env.BUILD_ID}")
            customImage.push()
        }
    }
}
`

#### Step 4 - Create Dockerfile
* On intellij created Dockerfile to source root of directory

#### Step 5 - Edited Dockerfile

Base image ubuntu 18.04
FROM ubuntu:18.04

Disable Prompt During Packages Installation
ARG DEBIAN_FRONTEND=noninteractive

Install java
RUN apt-get update -y
RUN apt-get install openjdk-11-jdk-headless -y

Copy the jar file into the container
COPY build/libs/basic_demo-0.1.0.jar .

Expose Port for the Application
EXPOSE 59001

Run the server
CMD java -cp basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001

#### Step 6 - Created a new pipeline
* Created a new pipeline called "CA5-part2".
* Defined the "Pipeline Script from SCM" and Git was chosen as the SCM.
* Indicated my repository URL, and additionally the path to the Jenkinsfile located at CA2 - part2 folder
* After this, I pressed "Build Now" to build the project/job and checked the time and success of building it.
* Everytinhg worked as intended. 

#### Step 7 - Commit final changes
* `§ git commit -a -m 'final commit'`
* `§ git push`

#### Step 8 - Tag the last commit to seal the project
* `§ git tag ca5-part2`
* `§ git push origin ca5-part2`

## Alternative Implementation

**Difference Between Jenkins vs CircleCI**
Jenkins is a continuous integration and continuous deployment server developed in Java language.  Jenkins is a 
cross-platform and leading award-winning continuous integration automation tool with main goals as building and 
testing software continuously and monitoring execution of jobs running externally. CircleCI is a continuous integration     
tool being used by software development teams to build, test, and deploy applications quicker and easier on different 
platforms. CircleCI supports all types of software tests, including web, mobile, desktop, and container environments.

**What is Jenkins?**
Continuous integration means triggering the collection of tests on non-developer machines whenever someone pushes code 
into any software or SDK source repository component. Jenkins offers continuous integration services for almost any 
combination of languages and source code repositories using pipelines. Its open-source and extension capability are the 
main reasons for its success. Jenkins was developed in 2004 and initially called Hudson later in 2011 renamed to Jenkins 
due to disputes with Oracle. Jenkins has 1200+ plugins available for extension of Jenkins functionality. Many companies 
use Jenkins; some of them are Facebook, Sony, Netflix, Tumblr, Yahoo, eBay, etc. Jenkins comes with easy installation, 
configuration, rich plugin availability, extensibility, and distributed builds to different computers.

**What is CircleCI?**
CircleCI provides easy setup and maintenance without any difficulties. It is a cloud-based system, which means no 
dedicated server is required, and there is no need for maintenance/administration of a server with a free plan, even for
business accounts. CircleCI can be integrated with GitHub, Amazon EC2, Appfog, dotCloud, etc., and used by many 
companies such as Facebook, Spotify, Kickstarter, RedBull, Harvest, and Teespring, etc. Community members most emphasize
CircleCI attributes such as Github integration, fast builds, easy setup, slack integration, Docker support, and great 
customer support. CircleCI comes with the following features such as quick setup, deep customization, smart 
notifications, fast support, and automatic parallelization. It caches requirements installation and third-party 
dependencies instead of the installation of the environments.

**Key Differences Between Jenkins and CircleCI**
Both Jenkins vs CircleCI are popular choices in the market; let us discuss some of the major Difference Between Jenkins 
and CircleCI:

* Jenkins can support multiple jobs by multi-threading, whereas CircleCI has built support for parallelism, which 
project settings can achieve.
* In Jenkins, builds are configured using web UI with settings stored in the Jenkins server, whereas, in CircleCI, jobs 
can be built using the “circle.yaml” file.
* CircleCI has the best feature for debugging, “Debug via SSH”, which makes debugging easier, whereas Jenkins, we need 
to debug by clicking on Jobs manually.
* CircleCI changes the environment without warning, which is an issue, whereas it will change only when the user 
instructs in Jenkins.
* In Jenkins, we can cache Docker images using a private server, whereas, in CircleCI, we can’t cache the Docker images.
* In Jenkins, secrets will be encrypted using Jenkins credentials and Plugin, whereas, in CircleCI, we don’t have 
security like Jenkins.

| **Basis**             | **Jenkins**                                                                          | **CircleCI**                                                                                     |
|-----------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Build Configuration   | In Jenkins, Builds are configured using Jenkins UI, and all settings of the jobs are stored on the Jenkins file system in the Jenkins server, which makes it difficult to share configuration details with the team or organization. By cloning Github or other source repositories can’t copy info stored in Jenkins. | In CircleCI, we can build all jobs within a single file called “circle.yaml”. It is easy as CI configuration will be like any other source code repos, which make it easy to share and backup. On CircleCI, only a few settings like secrets will be stored in an encrypted format. | 
| Build Machines Access | In Jenkins, the DevOps person or team need to maintain the Jenkins server by installing all dependencies and debug if any issues are there. Debugging will be a little bit difficult in Jenkins. | In CircleCI, we don’t need to maintain the server as every job will be started in a new environment where all the dependencies will be installed by default. Troubleshooting in it will be easy due to its ssh feature. DevOps team can do whatever they need on host os without effecting builds where developers can debug using its ssh feature.                                                       | 
| Web UI                | Jenkins web UI was originally developed in a different landscape of web technology which is still fairly dated. In it, every page load will need to render completely by the server, which makes the navigation experience a little slow and clumsy process. Web UI becomes slow or less responsive due to more number of plugins. | In CircleCI, Web UI is a single page web application that is developed using AJAX, HTML5 and new technologies to make the user experience fast and easy to use. CircleCI web UI will be changed frequently with improvements which makes it popular among users.        | 
| Plugins               | In Jenkins, Plugins plays an important role as we will be using plugins for most of the tasks, such as if we want to check out the Git repo, we need the GitHub plugin. These plugins are developed in Java which can be integrated with a number of plugins which generates web view in JSP pages. | In CircleCI, almost all core functionality is built-in, like checking out the source code from GitHub or Bitbucket, running jobs, storing artifacts, and parsing the output without any plugins. We can develop shell scripts to add any functionality as we required.                                 | 
| Containers and Docker | In Jenkins, we don’t have built-in support for Docker workflow; the user needs to install and make it available in the built environment. |In CircleCI, we have built-in support for Docker in the workflow, which can be accessed by adding in the services section in the “circle.yaml” file.| 
| Parallelism           | In Jenkins, we can run multiple jobs or tests at a time using multi-threading, but it may cause issues related to database and file systems which will be difficult to debug. | In CircleCI, It has the inbuilt facility to support parallelism, which can be done by changing project settings- using multiple containers at once. | 
| Environment Change    | In Jenkins, the environment will change with user permission and gives a warning if it changes. | In CircleCI, the environment will change without any warning, which leads to debugging more number of days. |

**Implementation**

* After some research I've opted to implement CircleCI as an Alternative to Jenkins. 

* 1st - Created an account with bitbucket
* 2nd - Imported my repository to Circle CI

Obs.: Due to high level of workload in the project I didn't go further with the implementation of the Alternative.