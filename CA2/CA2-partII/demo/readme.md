# Git Tutorial

## Alternative to Gradle (MAVEN)
### Gradle vs. Maven
**Gradle:** Gradle is an open-source tool that helps us to create software with mechanization. This tool is 
widely used for the creation of different kinds of software due to its high performance. It works on Java
and a Groovy-based Domain-Specific Language (DSL) for developing the project structure. Gradle supports 
the creation of mobile and web applications with testing and deploying on various platforms. With its 
functionality, it is preferred as an official tool for developing Android applications.

**Maven:** Maven is an open-source project management tool that helps us to create different software in the
lifecycle used by this tool. This tool focuses on the standardization (i.e.) development of the software
in a standard layout within a short duration of time. With this, we can create Java projects but is 
compatible to be used for other languages too. Maven uses Extensible Markup language(XML) for the 
structuring of the application.

| Basis               | Gradle                                                                               | Maven                                                                                    |
|---------------------|--------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------|
| Based on            | Gradle is based on developing domain-specific language projects                      | Maven is based on developing pure Java language-based software                           | 
| Configuration       | It uses a Groovy-based Domain-specific language(DSL) for creating project structure  | It uses Extensible Markup Language(XML) for creating project structure.                  | 
| Focuses on          | Developing applications by adding new features to them                               | Developing applications in a given time limit                                            | 
| Performance         | It performs better than maven as it optimized for tracking only current running task | It does not create local temporary files during software creation, and is hence – slower | 
| Java Compilation    | It avoids compilation                                                                | It is necessary to compile                                                               | 
| Usability           | It is a new tool, which requires users to spend a lot of time to get used to it      | This tool is a known tool for many users and is easily available                         | 
| Customization       | This tool is highly customizable as it supports a variety of IDE’s                   | This tool serves a limited amount of developers and is not that customizable             | 
| Languages supported | It supports software development in Java, C, C++, and Groovy                         | It supports software development in Java, Scala, C#, and Ruby                            |

**Advantages of Gradle:**
* Gradle allows us to write the build script with Java programing language.
* It is easy to use and maintain. 
* It supports dependency management 
* It provides high performance and scalable builds. 
* Gradle integration process is quite easier. 
* It supports a multi-project structure. 
* It is easy to migrate to Gradle from Maven or other build tools.

**Advantages of Maven:**
* Maven has enhanced dependency management. 
* In Maven, there is no need to store the binary libraries (third party) within the source control. 
* It efficiently manages the hierarchical dependency tree. 
* It makes the debugging process more straightforward. 
* It provides better co-operation among the source code, plugin, libraries, and the IDE. 
* It reduces the duplication within the project.


## Implementation of Maven
#### Step 1 - Create new directory (part2-alternative):
* `§ mkdir part2-alternative`

#### Step 2 - Change to the new directory:
* `§ cd part2-alternative`

#### Step 3 - Create a new readme.md file and add to repository:
* `§ touch readme.md`
* `§ git add .`
* `§ git commit -m ‘created readme file’`
* `§ git push`

#### Step 4 - Create Maven Boot Project and add to new directory
* Generated a new boot project with maven at the provided the link https://start.spring.io/.
Unziped the file inside the part2-alternative directory.  

#### Step 5 - Remove src file
* `§ cd part2-alternative`
* `§ rm -R src/`

#### Step 6 - Copy src, webpack and json files
* This step is exactly as the one done in CA2 readme.md in part2.

#### Step 7 - Run the application
* `§ ./mvnw spring-boot:run`
*  As expected, i've accessed the link http://localhost:8080/ and confirmed that it was empty

#### Step 8 - Add github plugins
* Accessed the provided link in Ca2 slides https://github.com/eirslett/frontend-maven-plugin and added to 
my pom file the pluggins

#### Step 9 - Update Json script
* Updated the script as told in slides

#### Step 10 - Build project
* `§ ./mvnw install`

#### Step 11 - Run the application
* `§ ./mvnw spring-boot:run`
*  At this point it is expected to work, so i've accessed http://localhost:8080/ and confirmed that it works.

#### Step 12 - Create task to copy jar files to a folder named "dist" and to delete generated webpack
* Added it to my pom

#### Step 13 - Check if all features works as intended
* `§ ./mvnw install`
* `§ ./mvnw clean`

#### Step 14 - Commit changes
* `§ git add .`
* `§ git commit -m ‘final commit with all implementations’`
* `§ git push`

#### Step 15 - Tag the last commit to seal the project
* `§ git tag ca2-part2-alternative`
* `§ git push origin ca2-part2-alternative`
